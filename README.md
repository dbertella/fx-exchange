# fx exchange

## Start
This readme assume `yarn` is installed on the local environment, but `npm` will work in a similar way.

Install dependencies
```
yarn install
```

To start the application an API key from [openexchangerates.org](https://openexchangerates.org/) is needed.
```
REACT_APP_API_KEY=OPEN-EXCHANGE-API-KEY yarn start
```

Running `yarn start` (without API key) will run the application with a set of stub data