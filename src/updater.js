// @flow
import type { CurrenciesKey, Currencies, State } from './types';

export const updateValue = (value: ?number) => (state: State) => ({
  fromValue: value,
});

export const updateToValue = (cb: (_: number) => number) => (state: State) => ({
  toValue: state.fromValue ? cb(state.fromValue) : null,
  valuta: cb(1),
});

export const initializeRatesAndValuta = (
  rates: Currencies,
  cb: (_: number) => number,
) => (state: State) => ({
  currencies: rates,
  valuta: cb(1),
});

export const updateFromCurrency = (value: string) => (state: State) => ({
  fromCurrency: value,
  toCurrency:
    value === state.toCurrency ? state.fromCurrency : state.toCurrency,
});

export const updateToCurrency = (value: string) => (state: State) => ({
  toCurrency: value,
  fromCurrency:
    value === state.fromCurrency ? state.toCurrency : state.fromCurrency,
});

export const updateOnSubmit = (
  fromCurrency: CurrenciesKey,
  toCurrency: CurrenciesKey,
  balance: Currencies,
  newFromBalance: number,
  toValue: ?number,
) => (state: State) => ({
  balance: {
    ...balance,
    [fromCurrency]: newFromBalance,
    [toCurrency]: balance[toCurrency] + toValue,
  },
  fromValue: null,
  toValue: null,
});
