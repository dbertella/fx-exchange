// @flow
export const APP_ID = process.env.REACT_APP_API_KEY || 'TEST_KEY';

export const API_URL = 'https://openexchangerates.org/api/';
