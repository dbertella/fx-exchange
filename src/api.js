// @flow
import { APP_ID, API_URL } from './config';
import { DATA_STUB } from './constants';

const queryParams = (params: { [string]: string }) =>
  Object.keys(params)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
    .join('&');

const params = queryParams({
  app_id: APP_ID,
  symbols: 'GBP,USD,EUR',
});

export const getJson = async (
  endpoint: string,
  options?: RequestOptions = {},
) => {
  // If application started without a valid API key serve stub data
  if (APP_ID === 'TEST_KEY') {
    return DATA_STUB[Math.floor(Math.random() * 3)];
  }
  const response = await fetch(`${API_URL}${endpoint}.json?${params}`);
  const json = await response.json();
  return json;
};
