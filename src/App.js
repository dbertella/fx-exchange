// @flow
import React, { Component } from 'react';
import fx from 'money';

import { POLL_DURATUION } from './constants';
import { getJson } from './api';
import Input from './components/Input';
import Select from './components/Select';
import Balance from './components/Balance';
import Rate from './components/Rate';
import {
  Widget,
  WidgetTitle,
  WidgetHeader,
  Section,
  Label,
  SubmitButton,
} from './components';
import {
  updateValue,
  updateToValue,
  initializeRatesAndValuta,
  updateFromCurrency,
  updateToCurrency,
  updateOnSubmit,
} from './updater';
import type { State } from './types';

type Props = {};
class App extends Component<Props, State> {
  state = {
    currencies: {},
    fromCurrency: 'GBP',
    toCurrency: 'EUR',
    fromValue: null,
    toValue: null,
    valuta: 0,
    balance: {
      GBP: 500,
      EUR: 100,
      USD: 100,
    },
  };

  componentDidMount() {
    this.apiCall();
  }

  pollingJson() {
    setTimeout(() => this.apiCall(), POLL_DURATUION);
  }

  componentWillUnmount() {
    clearTimeout(this.pollingJson);
  }

  apiCall = async () => {
    await this.pollingJson();
    const data = await getJson('latest');
    console.log('Latest rates updated at:', new Date().toString());
    fx.rates = data.rates;
    this.setState(initializeRatesAndValuta(data.rates, this.handleFx));
  };

  handleFx = (fromValue: number) =>
    fx(fromValue)
      .from(this.state.fromCurrency)
      .to(this.state.toCurrency);

  handleValue = (value: ?number) => {
    this.setState(updateValue(value), () => {
      this.setState(updateToValue(this.handleFx));
    });
  };

  handleFromCurrency = ({ target }: SyntheticInputEvent<*>) => {
    this.setState(updateFromCurrency(target.value), () => {
      this.setState(updateToValue(this.handleFx));
    });
  };

  handleToCurrency = ({ target }: SyntheticInputEvent<*>) => {
    this.setState(updateToCurrency(target.value), () => {
      this.setState(updateToValue(this.handleFx));
    });
  };

  handleSubmit = (e: SyntheticEvent<*>) => {
    e.preventDefault();
    const {
      balance,
      fromCurrency,
      fromValue,
      toCurrency,
      toValue,
    } = this.state;

    const newFromBalance =
      fromValue && toValue ? balance[fromCurrency] - fromValue : -1;
    if (newFromBalance >= 0) {
      this.setState(
        updateOnSubmit(
          fromCurrency,
          toCurrency,
          balance,
          newFromBalance,
          toValue,
        ),
      );
    }
  };

  render() {
    const {
      valuta,
      currencies,
      fromCurrency,
      fromValue,
      toCurrency,
      toValue,
      balance,
    } = this.state;
    const disableSubmit = !(
      fromValue && balance[fromCurrency] - fromValue >= 0
    );
    return (
      <Widget onSubmit={this.handleSubmit}>
        <WidgetHeader>
          <WidgetTitle>Exchange</WidgetTitle>
        </WidgetHeader>
        <Label htmlFor="fromValue">{fromCurrency}</Label>
        <Section>
          <Select
            name="fromCurrency"
            value={fromCurrency}
            onChange={this.handleFromCurrency}
            currencies={currencies}
            excludeCurrency={toCurrency}
          />
          <Input
            placeholder="0"
            name="fromValue"
            value={fromValue}
            onChange={this.handleValue}
            autoFocus
          />
        </Section>
        <Balance balance={balance} currency={fromCurrency} />
        <Section justify="center">
          <Rate from={fromCurrency} to={toCurrency} valuta={valuta} />
        </Section>
        <Label htmlFor="toValue">{toCurrency}</Label>
        <Section>
          <Select
            name="toCurrency"
            value={toCurrency}
            onChange={this.handleToCurrency}
            currencies={currencies}
            excludeCurrency={fromCurrency}
          />
          <Input placeholder="0" name="toValue" value={toValue} disabled />
        </Section>
        <Balance balance={balance} currency={toCurrency} />
        <Section justify="center">
          <SubmitButton disabled={disableSubmit}>Exchange</SubmitButton>
        </Section>
      </Widget>
    );
  }
}

export default App;
