// @flow
import React from 'react';
import styled from 'styled-components';
import NumericInput from 'react-number-input';

const StyledInput = styled(NumericInput)`
  flex: 1;
  height: 2.5rem;
  padding: 0 0.5rem;
  border: none;
  line-height: 2.5rem;
  font-size: 1rem;
  text-align: right;
`;

export default class Input extends React.Component<{}> {
  input: NumericInput;

  render() {
    return (
      <StyledInput
        {...this.props}
        innerRef={comp => (this.input = comp)}
        min={0}
        format="0,0[.]00"
      />
    );
  }
}
