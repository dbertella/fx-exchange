// @flow
import React from 'react';
import styled from 'styled-components';

import { CURRENCY_SYMBOL, MAX_DECIMAL } from '../constants';
import type { CurrenciesKey } from '../types';

const Styled = styled.div`
  display: inline-block;
  padding: 0.2rem;
  background: #fff;
  border: 1px solid #e7e7e7;
  border-radius: 0.2rem;
`;

type Props = {
  from: CurrenciesKey,
  to: CurrenciesKey,
  valuta: number,
};
const Rate = ({ from, to, valuta }: Props) => (
  <Styled>
    {CURRENCY_SYMBOL[from]}1:{' '}
    {`${CURRENCY_SYMBOL[to]}${valuta.toFixed(MAX_DECIMAL * 2)}`}
  </Styled>
);

export default Rate;
