// @flow
import React from 'react';
import styled from 'styled-components';

import type { Currencies, CurrenciesKey } from '../types';

const Styled = styled.select`
  height: 2.5rem;
  margin-right: 0.5rem;
  border-radius: 0;
  font-size: 0.875rem;
`;

const renderOptions = (excludeCurrency: string, currencies: Currencies) =>
  Object.keys(currencies)
    .filter(currency => currency)
    .map(currency => (
      <option key={currency} value={currency}>
        {currency}
      </option>
    ));

type Props = {
  currencies: Currencies,
  name: string,
  value: CurrenciesKey,
  excludeCurrency: CurrenciesKey,
  onChange: (e: SyntheticInputEvent<*>) => void,
};

const Select = ({ excludeCurrency, currencies, ...rest }: Props) => (
  <Styled {...rest}>{renderOptions(excludeCurrency, currencies)}</Styled>
);

export default Select;
