// @flow
import styled from 'styled-components';

export const Section = styled.div`
  display: flex;
  justify-content: ${({ justify }) => justify};
  margin-bottom: 0.5rem;
`;

export const Widget = styled.form`
  margin: 5rem auto 0;
  max-width: 100%;
  width: 30rem;
  padding: 1rem;
  font-size: 1rem;
  background: #f3f3f3;
  border: 1px solid #e7e7e7;
`;

export const WidgetHeader = styled(Section)`margin-bottom: 1rem;`;

export const WidgetTitle = styled.h3`margin: 0;`;

export const Label = styled.label`display: none;`;

export const SubmitButton = styled.button.attrs({
  type: 'submit'
})`
  padding: 0.5rem;
  font-size: 1rem;
`;