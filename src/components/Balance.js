// @flow
import React from 'react';
import styled from 'styled-components';

import { CURRENCY_SYMBOL, MAX_DECIMAL } from '../constants';
import type { CurrenciesKey, Currencies } from '../types';

const Styled = styled.div`
  color: #777;
  font-size: 0.875rem;
`;

type Props = {
  balance: Currencies,
  currency: CurrenciesKey
};
const Balance = ({balance, currency}: Props) => <Styled>
  Balance: {`${CURRENCY_SYMBOL[currency]} ${balance[currency].toFixed(MAX_DECIMAL)}`}
</Styled>

export default Balance;
