// @flow
export const CURRENCY_SYMBOL = {
  USD: '$',
  GBP: '£',
  EUR: '€',
};

export const MAX_DECIMAL = 2;

export const POLL_DURATUION = 10 * 1000;

// If REACT_APP_API_KEY is not defined stub data will be served instead
export const DATA_STUB = [
  {
    timestamp: 1506715200,
    base: 'USD',
    rates: { EUR: 0.846068, GBP: 0.746266, USD: 1 },
  },
  {
    timestamp: 1506715201,
    base: 'USD',
    rates: { EUR: 0.958366, GBP: 0.85321, USD: 1 },
  },
  {
    timestamp: 1506715202,
    base: 'USD',
    rates: { EUR: 0.87463, GBP: 0.802348, USD: 1 },
  },
];
